# Air Retracts safety valve for an RC model

The project is an Arduino source code for a sensor that can read the air tank pressure and activate the retracts servo when the pressure goes below certain threshold.

In large RC models (including jets) the retracts (landing gear) may be driven by pressurised air. In case if there is a pressure leak in the system, there is a risk that the pressure drops too low and the retracts can't be deployed at landing time. The results can be devasting to the model. To avoid this risk, the pressure sensor monitors the pressure in the tank and can deploy the retracts and lock them before it is too late. The sensor is independant of the radio type and does not use telemetry.

The sensor uses an inexpensinve automative pressure sensor that has been adopted for RC model application. Off the shelf 100PSI Pressure Sensor needs to be machined to remove unnecesary weight/metal and allow connecting 3mm air tube nipple that connects to the air tank. The sensor's electrical cable connects to the Arduino Nano board.

Any sensor that provides resistive output may be adapted; we have selected an automotive sensor. Despite its size and extra weight the sensor is simple, inexpensive and readily available. There are smaller sensors available but they will usually require tube adapaters and the costs multiply. Depending on the sensor resistance value, the divider resistor may need to be adjusted. The sensor resistance varies with the pressure applied. To achieve the best resolution for a given sensor, it is advisable to measure the sensor resistance at zero pressure and the max pressure (used in the model) and select the divider resistor that is equal to the mid pressure resistance.

Arduino Nano reads to the Receiver (RX) retracts channel PWM and writes to the retracts servo. The retract channel PWM value is a "pass through" to the servo unless the pressure drops too low. In that case the RX PWM value is overridden and teh output PWM towrds the servo is permanently set to "deployed" value. It can only be reset after landing by power cycling the sensor. In the "pass through" mode the sensor allow normal retract operation (controlled by the pilot).

| RX ch | Arduino | Sensor | Servo |
| ------ | ------ | ------ | ----- |
| black | GND | black | black |
| red | Vin | - | red |
| yellow | A4 | - | - |
| - | A1 | green | - |
| - | A3 | - | yellow |
| - | +5V | red | - |

| Arduino | part | Arduino | 
| ------ | ------ | ------ |
| A1 | divider resistor | +5V |

If the code is compiled and loaded to the Nano with DEBUG defined, the bord will provide messages to the console via USB.

Operations:
- with the sensor installed and connected, power on the model. The best is to have the TX switches in the wheels down mode, or set the mode to wheels down within 5s from powering on the RX. Nano LED is off
- after 5s the LED will start to blink and initialise itself
- pump the air tank to the desired max pressure (if not already done). Max value will be stored. Ready to go flying

Please test the sensor operations before going flying for 'flow through' operation, i.e. pilot is in full control of the retracts and for emergency deploy. The later can be tested by slowly releasing the pressure from the tank while watching the pressure indicator. At mid pressure the sensor should deploy the ratracts and lock them down (no input from the pilot can override it).

The sensor has been built and successfuly flown multiple times. The code has got built in the practical experience gain including histeresis, resistance to interferance, etc. The sensor automatically adapts to individual model characteristics and stores the calibration in the Nano eeprom. However, the sensor's code is provided with no warranties of any kind and we do not take responsibility for any issues resulting from using this code, the sensor nor accuracy of the information provided here. If you can not accept it, please do not use the code.

![alt text](Sensor_100PSI.png "sensor1")
![alt text](Sensor_In.jpg "sensor2")
![alt text](Sensor_graph.png "graph")



