#include <EEPROM.h>
#include <Servo.h>

// init debug
#define DEBUG
// debug in the main loop
#define DEBUGR

#define RF_VER "1.0.1"

// main loop interval in ms
#define MINT 200

// RX const
#define RX_IN 4
// RX wait in ms
#define WHEELS_DOWN_WAIT 5000
#define RXATTEMPTS 10

// servo const
#define SERVO_OUT 3 // servo pin

// sensor pin
#define PSI_IN A1
#define A2PSI 0.147579693
#define MIN_CUT_PSI 666 // large automative sensor 100psi
#define MIN_CUT_HISTERESIS 20

// eeprom locations
// trigger pressure for wheels down memory address
#define EE_Trigger 111
// eeprom updates, value different than 0 indicates that eeprom was used
#define EE_used 444

// variables
Servo myservo;
unsigned long lastTime = 0;
unsigned int rx = 0;  // RX channel value
unsigned int rx_down = 0; // RX value for wheels down
unsigned int rx_up = 0; // RX value for wheels up
boolean lowHigh = true; // true if RX value for wheels down is lower than wheels up
unsigned int rx_min = 0; // learnt minimum RX value
unsigned int rx_max = 0; // learnt max RX value
unsigned int apsi = 0; // analog psi value
unsigned int psi = 0; // real psi value
unsigned int apsi_min = 0; // learnt min psi value
unsigned int apsi_max = 100; // learnt max psi value
unsigned int apsi_med = 50; // learnt med psi value
unsigned int apsi_cut = 50; // psi trigger value
byte apsi_cut_times = 0; // learnt max psi value
boolean apsi_low_times = 5;  // 
boolean blind = false;  // true if wheels have been dropped due to low pressure
boolean active = false;  // true if wheels have been dropped due to low pressure

boolean LED = false;



/* ******a2psi*************a2psi**********a2psi***********a2psi*********a2psi******
 * help function that translates pressure sensor analog value to psi
 */
float a2psi (unsigned int apsi)  {
  return apsi * A2PSI;
}

/* ******readRX*************readRX******readRX******readRX******readRX******readRX********
 * help function that quickly reads average RX value over number of attempts (max 255)
 */
unsigned int readRX (byte times)   {
  unsigned long rx = 0;
  for (byte i = 0; i < times; i++) {
    rx += pulseIn(RX_IN, HIGH);
  }
  return rx/times;
}

/* ******readPressure*************readPressure******readPressure******readPressure******readPressure******
 * help function that quickly reads average Pressure value over number of attempts (max 255)
 */
unsigned int readPressure(byte times)   {
  unsigned long psi = 0;
  for (byte i = 0; i < times; i++) {
    psi += analogRead(PSI_IN);
  }
  return psi/times;
}

/* ******blink******blink******blink******blink******blink******blink******blink******blink
 * blink LED
 */
void blink(void)   {
  
  if (!LED)  {
    digitalWrite(LED_BUILTIN, HIGH);
    LED = true;
  } else  {
    digitalWrite(LED_BUILTIN, LOW);
    LED = false;
  }
  
}



/*
 * SETUP **********SETUP*********SETUP**********SETUP**********SETUP**********
 */
void setup() {
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

                              #ifdef DEBUG
                                Serial.begin(115200);
                                Serial.println("");
                                Serial.println("");
                                Serial.println("Retracts Failsafe:");
                                Serial.print("version: ");Serial.println(RF_VER);
                                Serial.print("polling [ms]: ");Serial.println(MINT);
                              #endif
  
      // init RX port
  pinMode(RX_IN, INPUT);
  delay(MINT);
                              #ifdef DEBUG
                                Serial.print("Waiting for RX to come online.");
                              #endif

      // wait for RX to come online
  while(rx < 100)  {   // expected rx value is between 800-2000, 100 allows for noise
    rx = pulseIn(RX_IN, HIGH);
                              #ifdef DEBUG
                                Serial.print(".");
                              #endif
    delay(MINT);
    blink();
  }
                              #ifdef DEBUG
                                Serial.println();
                                Serial.println("RX is online or in Failsafe holding old value.");
                              #endif

      // allow 5s delay for the pilot to set channel to wheels down
  digitalWrite(LED_BUILTIN, LOW); LED = false;
  delay(WHEELS_DOWN_WAIT);
  blink();
      // read RX and set to wheels down
  rx_down = readRX(RXATTEMPTS);  
      // init servo port
  myservo.attach(SERVO_OUT);  // attached servo on pin 10
  myservo.writeMicroseconds(rx_down);  // immediately set to RX value

                              #ifdef DEBUG
                                Serial.print("EEPROM values");
                                if( EEPROM.read(EE_used) != 255)  {
                                  Serial.println(" used:");
                                  Serial.print("Trigger pressure: ");Serial.println( EEPROM.read(EE_Trigger) );
                                } else  {
                                  Serial.println(" not defined.");
                                }
                              #endif
  
  // init sensor port
  apsi = readPressure(3);
                              #ifdef DEBUG
                                Serial.print("initial pressure: ");Serial.print( apsi );Serial.print(", ");Serial.print( a2psi(apsi) );
                                Serial.println(" psi.");
                                Serial.println("Exited Init.");
                                Serial.println("-------------------------->");
                              #endif                               
}






/*
 * MAIN loop ******MAIN************MAIN*************MAIN***************MAIN*******MAIN******
 */
void loop() {
 
  if ( (millis() > lastTime + MINT) && !blind) {  

      blink();
      
              // read Pressure and recalibrate
      apsi = readPressure(5);
                              #ifdef DEBUGR
                                Serial.print("apsi: ");Serial.print( apsi );
                              #endif          
                                    
      if (apsi > MIN_CUT_PSI)  {
          active = true;
      }
                              
      if (apsi < (MIN_CUT_PSI - MIN_CUT_HISTERESIS) )  {  // make it lower than cutoff for 0.5 second to avoid jitter
        
          apsi_cut_times+=1;
          
          if (apsi_cut_times > apsi_low_times && active) {
            blind = true;
            myservo.writeMicroseconds(rx_down);  // wheels down !
                                #ifdef DEBUGR
                                  Serial.println( "" );
                                  Serial.println("Wheels Down!!!");  
                                #endif              
          }
        
      }   else    {
          apsi_cut_times = 0;
      }

      if (!blind) {
          rx = readRX(3); // average from 3 reads, less jitter on servo
          myservo.writeMicroseconds(rx);  // pass through from RX
                              #ifdef DEBUGR
                                Serial.print(" RX[us]: ");Serial.println(rx);  
                              #endif        
      }
      lastTime = millis(); // close loop      
    }
  
}
